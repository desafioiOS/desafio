# Desafio Timeline iOS

Primeiramente obrigado por estar aqui! E querer trabalhar no Itaú Unibanco!

Neste desafio queremos entender um pouco mais do seu conhecimento em desenvolvimento iOS, método de avaliação é simples, tendo seu código em mãos iremos validar a solução que você desenvolveu para o desafio

# Requisitos

Swift 5 ou superior

iOS 10.3 ou superior

Preferencialmente não usar Frameworks externos


# Desafio

- Nivel 1


> O itaú deseja fazer um novo app para mostrar os lançamentos da conta corrente, o que precisa ser feito :

    Apresentar os dados de lançamentos da API ( https://gitlab.com/desafioiOS/rest-api )

*( exemplo conceitual https://dribbble.com/shots/3179815-Mobile-Banking-App)*

    Ordenar os lançamentos a partir do mês

- Nivel 2

> Lançamos uma versão inicial e foi um sucesso! mas os clientes procuram novas funcionalidades para melhorar sua experiência, o PO priorizou as seguintes tasks : 

    Detalhamento de lançamentos em outra tela
    
    Separar os lançamentos por mês
    
    Mostrar o balanço de gastos de cada mês
    
    Testes Unitarios

- Nivel 3

> Precisamos criar novas features mas equipe Cresceu, aplique também praticas que agilizem nosso desenvolvimento, utilize os seguintes conceitos 

    Criar novas categorias ( https://gitlab.com/desafioiOS/rest-api - /categorias )

    Alterar as categorias dos lançamentos ( https://gitlab.com/desafioiOS/rest-api - /lançamentos )

    Poder Filtrar por Mês
    
    Testes de UI
    
    Utilizar Arquitetura MVVM
    
    Utilizar Coordinator
    
    Redimensionável (ipad e iphone)


**Bom Desempenho!**

# QueremosVoceNoItau
